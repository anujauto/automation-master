#!/bin/bash
 
cd $HOME/jmeter/apache-jmeter-5.4.3/bin
sh jmeter -n -t 'Test_plan_files/Test_plan_collection_sponsor_type_search.jmx' -l "logs/new_$(date +%Y%m%d_%H%M%S).csv" -e -o "output/collection_sponsor_type_search_$(date +%Y%m%d_%H%M%S)"