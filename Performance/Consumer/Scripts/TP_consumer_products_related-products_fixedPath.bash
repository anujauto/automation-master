#!/bin/bash
 
cd $HOME/jmeter/apache-jmeter-5.4.3/bin
sh jmeter -n -t 'Test_plan_files/Test_plan_consumer_products_related-products.jmx' -l "logs/new_$(date +%Y%m%d_%H%M%S).csv" -e -o "output/consumer_products_related-products_$(date +%Y%m%d_%H%M%S)"