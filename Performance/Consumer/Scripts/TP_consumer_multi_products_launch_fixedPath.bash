#!/bin/bash
 
cd $HOME/jmeter/apache-jmeter-5.4.3/bin
sh jmeter -n -t 'Test_plan_files/Test_plan_consumer_multi_products_launch.jmx' -l "logs/new_$(date +%Y%m%d_%H%M%S).csv" -e -o "output/consumer_multi_products_launch_$(date +%Y%m%d_%H%M%S)"