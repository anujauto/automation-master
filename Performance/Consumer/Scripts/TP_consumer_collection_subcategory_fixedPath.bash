#!/bin/bash
 
cd $HOME/jmeter/apache-jmeter-5.4.3/bin
sh jmeter -n -t 'Test_plan_files/Test_plan_consumer_collection_subcategory.jmx' -l "logs/new_$(date +%Y%m%d_%H%M%S).csv" -e -o "output/consumer_collection_subcategory_$(date +%Y%m%d_%H%M%S)"