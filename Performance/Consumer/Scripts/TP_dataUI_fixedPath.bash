#!/bin/bash
 
cd $HOME/jmeter/apache-jmeter-5.4.3/bin
sh jmeter -n -t 'Test_plan_files/Test_plan_dataUI.jmx' -l "logs/new_$(date +%Y%m%d_%H%M%S).csv" -e -o "output/dataUI_$(date +%Y%m%d_%H%M%S)"