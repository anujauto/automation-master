#!/bin/bash
 
cd $HOME/jmeter/apache-jmeter-5.4.3/bin
sh jmeter -n -t 'Test_plan_files/Test_plan_baskets.jmx' -l "logs/new_$(date +%Y%m%d_%H%M%S).csv" -e -o "output/baskets_$(date +%Y%m%d_%H%M%S)"