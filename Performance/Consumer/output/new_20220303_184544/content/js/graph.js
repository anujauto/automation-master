/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 29.0, "minX": 0.0, "maxY": 75115.0, "series": [{"data": [[0.0, 29.0], [0.1, 71.0], [0.2, 72.0], [0.3, 73.0], [0.4, 73.0], [0.5, 73.0], [0.6, 73.0], [0.7, 74.0], [0.8, 74.0], [0.9, 74.0], [1.0, 74.0], [1.1, 74.0], [1.2, 74.0], [1.3, 75.0], [1.4, 75.0], [1.5, 75.0], [1.6, 75.0], [1.7, 75.0], [1.8, 75.0], [1.9, 75.0], [2.0, 75.0], [2.1, 75.0], [2.2, 75.0], [2.3, 75.0], [2.4, 76.0], [2.5, 76.0], [2.6, 76.0], [2.7, 76.0], [2.8, 76.0], [2.9, 76.0], [3.0, 76.0], [3.1, 76.0], [3.2, 76.0], [3.3, 76.0], [3.4, 76.0], [3.5, 76.0], [3.6, 76.0], [3.7, 76.0], [3.8, 76.0], [3.9, 76.0], [4.0, 77.0], [4.1, 77.0], [4.2, 77.0], [4.3, 77.0], [4.4, 77.0], [4.5, 77.0], [4.6, 77.0], [4.7, 77.0], [4.8, 77.0], [4.9, 77.0], [5.0, 77.0], [5.1, 77.0], [5.2, 77.0], [5.3, 77.0], [5.4, 77.0], [5.5, 77.0], [5.6, 77.0], [5.7, 77.0], [5.8, 77.0], [5.9, 77.0], [6.0, 77.0], [6.1, 77.0], [6.2, 77.0], [6.3, 77.0], [6.4, 78.0], [6.5, 78.0], [6.6, 78.0], [6.7, 78.0], [6.8, 78.0], [6.9, 78.0], [7.0, 78.0], [7.1, 78.0], [7.2, 78.0], [7.3, 78.0], [7.4, 78.0], [7.5, 78.0], [7.6, 78.0], [7.7, 78.0], [7.8, 78.0], [7.9, 78.0], [8.0, 78.0], [8.1, 78.0], [8.2, 78.0], [8.3, 78.0], [8.4, 78.0], [8.5, 78.0], [8.6, 78.0], [8.7, 78.0], [8.8, 78.0], [8.9, 78.0], [9.0, 78.0], [9.1, 78.0], [9.2, 78.0], [9.3, 78.0], [9.4, 79.0], [9.5, 79.0], [9.6, 79.0], [9.7, 79.0], [9.8, 79.0], [9.9, 79.0], [10.0, 79.0], [10.1, 79.0], [10.2, 79.0], [10.3, 79.0], [10.4, 79.0], [10.5, 79.0], [10.6, 79.0], [10.7, 79.0], [10.8, 79.0], [10.9, 79.0], [11.0, 79.0], [11.1, 79.0], [11.2, 79.0], [11.3, 79.0], [11.4, 79.0], [11.5, 79.0], [11.6, 79.0], [11.7, 79.0], [11.8, 79.0], [11.9, 79.0], [12.0, 79.0], [12.1, 79.0], [12.2, 79.0], [12.3, 79.0], [12.4, 79.0], [12.5, 79.0], [12.6, 79.0], [12.7, 79.0], [12.8, 79.0], [12.9, 80.0], [13.0, 80.0], [13.1, 80.0], [13.2, 80.0], [13.3, 80.0], [13.4, 80.0], [13.5, 80.0], [13.6, 80.0], [13.7, 80.0], [13.8, 80.0], [13.9, 80.0], [14.0, 80.0], [14.1, 80.0], [14.2, 80.0], [14.3, 80.0], [14.4, 80.0], [14.5, 80.0], [14.6, 80.0], [14.7, 80.0], [14.8, 80.0], [14.9, 80.0], [15.0, 80.0], [15.1, 80.0], [15.2, 80.0], [15.3, 80.0], [15.4, 80.0], [15.5, 80.0], [15.6, 80.0], [15.7, 80.0], [15.8, 80.0], [15.9, 80.0], [16.0, 80.0], [16.1, 80.0], [16.2, 80.0], [16.3, 80.0], [16.4, 80.0], [16.5, 80.0], [16.6, 80.0], [16.7, 80.0], [16.8, 80.0], [16.9, 80.0], [17.0, 80.0], [17.1, 81.0], [17.2, 81.0], [17.3, 81.0], [17.4, 81.0], [17.5, 81.0], [17.6, 81.0], [17.7, 81.0], [17.8, 81.0], [17.9, 81.0], [18.0, 81.0], [18.1, 81.0], [18.2, 81.0], [18.3, 81.0], [18.4, 81.0], [18.5, 81.0], [18.6, 81.0], [18.7, 81.0], [18.8, 81.0], [18.9, 81.0], [19.0, 81.0], [19.1, 81.0], [19.2, 81.0], [19.3, 81.0], [19.4, 81.0], [19.5, 81.0], [19.6, 81.0], [19.7, 81.0], [19.8, 81.0], [19.9, 81.0], [20.0, 81.0], [20.1, 81.0], [20.2, 81.0], [20.3, 81.0], [20.4, 81.0], [20.5, 81.0], [20.6, 81.0], [20.7, 81.0], [20.8, 81.0], [20.9, 81.0], [21.0, 81.0], [21.1, 81.0], [21.2, 81.0], [21.3, 81.0], [21.4, 81.0], [21.5, 82.0], [21.6, 82.0], [21.7, 82.0], [21.8, 82.0], [21.9, 82.0], [22.0, 82.0], [22.1, 82.0], [22.2, 82.0], [22.3, 82.0], [22.4, 82.0], [22.5, 82.0], [22.6, 82.0], [22.7, 82.0], [22.8, 82.0], [22.9, 82.0], [23.0, 82.0], [23.1, 82.0], [23.2, 82.0], [23.3, 82.0], [23.4, 82.0], [23.5, 82.0], [23.6, 82.0], [23.7, 82.0], [23.8, 82.0], [23.9, 82.0], [24.0, 82.0], [24.1, 82.0], [24.2, 82.0], [24.3, 82.0], [24.4, 82.0], [24.5, 82.0], [24.6, 82.0], [24.7, 82.0], [24.8, 82.0], [24.9, 82.0], [25.0, 82.0], [25.1, 82.0], [25.2, 82.0], [25.3, 82.0], [25.4, 82.0], [25.5, 82.0], [25.6, 82.0], [25.7, 82.0], [25.8, 82.0], [25.9, 82.0], [26.0, 82.0], [26.1, 82.0], [26.2, 83.0], [26.3, 83.0], [26.4, 83.0], [26.5, 83.0], [26.6, 83.0], [26.7, 83.0], [26.8, 83.0], [26.9, 83.0], [27.0, 83.0], [27.1, 83.0], [27.2, 83.0], [27.3, 83.0], [27.4, 83.0], [27.5, 83.0], [27.6, 83.0], [27.7, 83.0], [27.8, 83.0], [27.9, 83.0], [28.0, 83.0], [28.1, 83.0], [28.2, 83.0], [28.3, 83.0], [28.4, 83.0], [28.5, 83.0], [28.6, 83.0], [28.7, 83.0], [28.8, 83.0], [28.9, 83.0], [29.0, 83.0], [29.1, 83.0], [29.2, 83.0], [29.3, 83.0], [29.4, 83.0], [29.5, 83.0], [29.6, 83.0], [29.7, 83.0], [29.8, 83.0], [29.9, 83.0], [30.0, 83.0], [30.1, 83.0], [30.2, 83.0], [30.3, 83.0], [30.4, 83.0], [30.5, 83.0], [30.6, 83.0], [30.7, 83.0], [30.8, 84.0], [30.9, 84.0], [31.0, 84.0], [31.1, 84.0], [31.2, 84.0], [31.3, 84.0], [31.4, 84.0], [31.5, 84.0], [31.6, 84.0], [31.7, 84.0], [31.8, 84.0], [31.9, 84.0], [32.0, 84.0], [32.1, 84.0], [32.2, 84.0], [32.3, 84.0], [32.4, 84.0], [32.5, 84.0], [32.6, 84.0], [32.7, 84.0], [32.8, 84.0], [32.9, 84.0], [33.0, 84.0], [33.1, 84.0], [33.2, 84.0], [33.3, 84.0], [33.4, 84.0], [33.5, 84.0], [33.6, 84.0], [33.7, 84.0], [33.8, 84.0], [33.9, 84.0], [34.0, 84.0], [34.1, 84.0], [34.2, 84.0], [34.3, 84.0], [34.4, 84.0], [34.5, 84.0], [34.6, 84.0], [34.7, 84.0], [34.8, 84.0], [34.9, 84.0], [35.0, 84.0], [35.1, 84.0], [35.2, 84.0], [35.3, 85.0], [35.4, 85.0], [35.5, 85.0], [35.6, 85.0], [35.7, 85.0], [35.8, 85.0], [35.9, 85.0], [36.0, 85.0], [36.1, 85.0], [36.2, 85.0], [36.3, 85.0], [36.4, 85.0], [36.5, 85.0], [36.6, 85.0], [36.7, 85.0], [36.8, 85.0], [36.9, 85.0], [37.0, 85.0], [37.1, 85.0], [37.2, 85.0], [37.3, 85.0], [37.4, 85.0], [37.5, 85.0], [37.6, 85.0], [37.7, 85.0], [37.8, 85.0], [37.9, 85.0], [38.0, 85.0], [38.1, 85.0], [38.2, 85.0], [38.3, 85.0], [38.4, 85.0], [38.5, 85.0], [38.6, 85.0], [38.7, 85.0], [38.8, 85.0], [38.9, 85.0], [39.0, 85.0], [39.1, 85.0], [39.2, 85.0], [39.3, 85.0], [39.4, 85.0], [39.5, 85.0], [39.6, 85.0], [39.7, 86.0], [39.8, 86.0], [39.9, 86.0], [40.0, 86.0], [40.1, 86.0], [40.2, 86.0], [40.3, 86.0], [40.4, 86.0], [40.5, 86.0], [40.6, 86.0], [40.7, 86.0], [40.8, 86.0], [40.9, 86.0], [41.0, 86.0], [41.1, 86.0], [41.2, 86.0], [41.3, 86.0], [41.4, 86.0], [41.5, 86.0], [41.6, 86.0], [41.7, 86.0], [41.8, 86.0], [41.9, 86.0], [42.0, 86.0], [42.1, 86.0], [42.2, 86.0], [42.3, 86.0], [42.4, 86.0], [42.5, 86.0], [42.6, 86.0], [42.7, 86.0], [42.8, 86.0], [42.9, 86.0], [43.0, 86.0], [43.1, 86.0], [43.2, 86.0], [43.3, 86.0], [43.4, 86.0], [43.5, 86.0], [43.6, 87.0], [43.7, 87.0], [43.8, 87.0], [43.9, 87.0], [44.0, 87.0], [44.1, 87.0], [44.2, 87.0], [44.3, 87.0], [44.4, 87.0], [44.5, 87.0], [44.6, 87.0], [44.7, 87.0], [44.8, 87.0], [44.9, 87.0], [45.0, 87.0], [45.1, 87.0], [45.2, 87.0], [45.3, 87.0], [45.4, 87.0], [45.5, 87.0], [45.6, 87.0], [45.7, 87.0], [45.8, 87.0], [45.9, 87.0], [46.0, 87.0], [46.1, 87.0], [46.2, 87.0], [46.3, 87.0], [46.4, 87.0], [46.5, 87.0], [46.6, 87.0], [46.7, 87.0], [46.8, 87.0], [46.9, 87.0], [47.0, 87.0], [47.1, 87.0], [47.2, 87.0], [47.3, 87.0], [47.4, 88.0], [47.5, 88.0], [47.6, 88.0], [47.7, 88.0], [47.8, 88.0], [47.9, 88.0], [48.0, 88.0], [48.1, 88.0], [48.2, 88.0], [48.3, 88.0], [48.4, 88.0], [48.5, 88.0], [48.6, 88.0], [48.7, 88.0], [48.8, 88.0], [48.9, 88.0], [49.0, 88.0], [49.1, 88.0], [49.2, 88.0], [49.3, 88.0], [49.4, 88.0], [49.5, 88.0], [49.6, 88.0], [49.7, 88.0], [49.8, 88.0], [49.9, 88.0], [50.0, 88.0], [50.1, 88.0], [50.2, 88.0], [50.3, 88.0], [50.4, 88.0], [50.5, 88.0], [50.6, 88.0], [50.7, 88.0], [50.8, 89.0], [50.9, 89.0], [51.0, 89.0], [51.1, 89.0], [51.2, 89.0], [51.3, 89.0], [51.4, 89.0], [51.5, 89.0], [51.6, 89.0], [51.7, 89.0], [51.8, 89.0], [51.9, 89.0], [52.0, 89.0], [52.1, 89.0], [52.2, 89.0], [52.3, 89.0], [52.4, 89.0], [52.5, 89.0], [52.6, 89.0], [52.7, 89.0], [52.8, 89.0], [52.9, 89.0], [53.0, 89.0], [53.1, 89.0], [53.2, 89.0], [53.3, 89.0], [53.4, 89.0], [53.5, 89.0], [53.6, 89.0], [53.7, 89.0], [53.8, 89.0], [53.9, 90.0], [54.0, 90.0], [54.1, 90.0], [54.2, 90.0], [54.3, 90.0], [54.4, 90.0], [54.5, 90.0], [54.6, 90.0], [54.7, 90.0], [54.8, 90.0], [54.9, 90.0], [55.0, 90.0], [55.1, 90.0], [55.2, 90.0], [55.3, 90.0], [55.4, 90.0], [55.5, 90.0], [55.6, 90.0], [55.7, 90.0], [55.8, 90.0], [55.9, 90.0], [56.0, 90.0], [56.1, 90.0], [56.2, 90.0], [56.3, 90.0], [56.4, 90.0], [56.5, 90.0], [56.6, 91.0], [56.7, 91.0], [56.8, 91.0], [56.9, 91.0], [57.0, 91.0], [57.1, 91.0], [57.2, 91.0], [57.3, 91.0], [57.4, 91.0], [57.5, 91.0], [57.6, 91.0], [57.7, 91.0], [57.8, 91.0], [57.9, 91.0], [58.0, 91.0], [58.1, 91.0], [58.2, 91.0], [58.3, 91.0], [58.4, 91.0], [58.5, 91.0], [58.6, 91.0], [58.7, 91.0], [58.8, 91.0], [58.9, 91.0], [59.0, 92.0], [59.1, 92.0], [59.2, 92.0], [59.3, 92.0], [59.4, 92.0], [59.5, 92.0], [59.6, 92.0], [59.7, 92.0], [59.8, 92.0], [59.9, 92.0], [60.0, 92.0], [60.1, 92.0], [60.2, 92.0], [60.3, 92.0], [60.4, 92.0], [60.5, 92.0], [60.6, 92.0], [60.7, 92.0], [60.8, 92.0], [60.9, 92.0], [61.0, 92.0], [61.1, 92.0], [61.2, 92.0], [61.3, 93.0], [61.4, 93.0], [61.5, 93.0], [61.6, 93.0], [61.7, 93.0], [61.8, 93.0], [61.9, 93.0], [62.0, 93.0], [62.1, 93.0], [62.2, 93.0], [62.3, 93.0], [62.4, 93.0], [62.5, 93.0], [62.6, 93.0], [62.7, 93.0], [62.8, 93.0], [62.9, 93.0], [63.0, 93.0], [63.1, 93.0], [63.2, 93.0], [63.3, 93.0], [63.4, 93.0], [63.5, 93.0], [63.6, 94.0], [63.7, 94.0], [63.8, 94.0], [63.9, 94.0], [64.0, 94.0], [64.1, 94.0], [64.2, 94.0], [64.3, 94.0], [64.4, 94.0], [64.5, 94.0], [64.6, 94.0], [64.7, 94.0], [64.8, 94.0], [64.9, 94.0], [65.0, 94.0], [65.1, 94.0], [65.2, 94.0], [65.3, 94.0], [65.4, 94.0], [65.5, 95.0], [65.6, 95.0], [65.7, 95.0], [65.8, 95.0], [65.9, 95.0], [66.0, 95.0], [66.1, 95.0], [66.2, 95.0], [66.3, 95.0], [66.4, 95.0], [66.5, 95.0], [66.6, 95.0], [66.7, 95.0], [66.8, 95.0], [66.9, 95.0], [67.0, 95.0], [67.1, 95.0], [67.2, 96.0], [67.3, 96.0], [67.4, 96.0], [67.5, 96.0], [67.6, 96.0], [67.7, 96.0], [67.8, 96.0], [67.9, 96.0], [68.0, 96.0], [68.1, 96.0], [68.2, 96.0], [68.3, 96.0], [68.4, 96.0], [68.5, 96.0], [68.6, 96.0], [68.7, 96.0], [68.8, 96.0], [68.9, 97.0], [69.0, 97.0], [69.1, 97.0], [69.2, 97.0], [69.3, 97.0], [69.4, 97.0], [69.5, 97.0], [69.6, 97.0], [69.7, 97.0], [69.8, 97.0], [69.9, 97.0], [70.0, 97.0], [70.1, 97.0], [70.2, 97.0], [70.3, 97.0], [70.4, 98.0], [70.5, 98.0], [70.6, 98.0], [70.7, 98.0], [70.8, 98.0], [70.9, 98.0], [71.0, 98.0], [71.1, 98.0], [71.2, 98.0], [71.3, 98.0], [71.4, 98.0], [71.5, 98.0], [71.6, 98.0], [71.7, 98.0], [71.8, 99.0], [71.9, 99.0], [72.0, 99.0], [72.1, 99.0], [72.2, 99.0], [72.3, 99.0], [72.4, 99.0], [72.5, 99.0], [72.6, 99.0], [72.7, 99.0], [72.8, 99.0], [72.9, 99.0], [73.0, 99.0], [73.1, 100.0], [73.2, 100.0], [73.3, 100.0], [73.4, 100.0], [73.5, 100.0], [73.6, 100.0], [73.7, 100.0], [73.8, 100.0], [73.9, 100.0], [74.0, 100.0], [74.1, 100.0], [74.2, 101.0], [74.3, 101.0], [74.4, 101.0], [74.5, 101.0], [74.6, 101.0], [74.7, 101.0], [74.8, 101.0], [74.9, 101.0], [75.0, 101.0], [75.1, 102.0], [75.2, 102.0], [75.3, 102.0], [75.4, 102.0], [75.5, 102.0], [75.6, 102.0], [75.7, 102.0], [75.8, 102.0], [75.9, 102.0], [76.0, 102.0], [76.1, 103.0], [76.2, 103.0], [76.3, 103.0], [76.4, 103.0], [76.5, 103.0], [76.6, 103.0], [76.7, 103.0], [76.8, 103.0], [76.9, 103.0], [77.0, 104.0], [77.1, 104.0], [77.2, 104.0], [77.3, 104.0], [77.4, 104.0], [77.5, 104.0], [77.6, 104.0], [77.7, 104.0], [77.8, 105.0], [77.9, 105.0], [78.0, 105.0], [78.1, 105.0], [78.2, 105.0], [78.3, 105.0], [78.4, 105.0], [78.5, 106.0], [78.6, 106.0], [78.7, 106.0], [78.8, 106.0], [78.9, 106.0], [79.0, 106.0], [79.1, 106.0], [79.2, 106.0], [79.3, 107.0], [79.4, 107.0], [79.5, 107.0], [79.6, 107.0], [79.7, 107.0], [79.8, 107.0], [79.9, 107.0], [80.0, 108.0], [80.1, 108.0], [80.2, 108.0], [80.3, 108.0], [80.4, 108.0], [80.5, 108.0], [80.6, 109.0], [80.7, 109.0], [80.8, 109.0], [80.9, 109.0], [81.0, 109.0], [81.1, 109.0], [81.2, 110.0], [81.3, 110.0], [81.4, 110.0], [81.5, 110.0], [81.6, 110.0], [81.7, 110.0], [81.8, 111.0], [81.9, 111.0], [82.0, 111.0], [82.1, 111.0], [82.2, 111.0], [82.3, 112.0], [82.4, 112.0], [82.5, 112.0], [82.6, 112.0], [82.7, 112.0], [82.8, 113.0], [82.9, 113.0], [83.0, 113.0], [83.1, 113.0], [83.2, 113.0], [83.3, 114.0], [83.4, 114.0], [83.5, 114.0], [83.6, 114.0], [83.7, 114.0], [83.8, 115.0], [83.9, 115.0], [84.0, 115.0], [84.1, 115.0], [84.2, 116.0], [84.3, 116.0], [84.4, 116.0], [84.5, 116.0], [84.6, 117.0], [84.7, 117.0], [84.8, 117.0], [84.9, 117.0], [85.0, 117.0], [85.1, 118.0], [85.2, 118.0], [85.3, 118.0], [85.4, 118.0], [85.5, 119.0], [85.6, 119.0], [85.7, 119.0], [85.8, 119.0], [85.9, 120.0], [86.0, 120.0], [86.1, 120.0], [86.2, 120.0], [86.3, 121.0], [86.4, 121.0], [86.5, 121.0], [86.6, 121.0], [86.7, 122.0], [86.8, 122.0], [86.9, 122.0], [87.0, 122.0], [87.1, 123.0], [87.2, 123.0], [87.3, 123.0], [87.4, 124.0], [87.5, 124.0], [87.6, 124.0], [87.7, 125.0], [87.8, 125.0], [87.9, 125.0], [88.0, 126.0], [88.1, 126.0], [88.2, 126.0], [88.3, 127.0], [88.4, 127.0], [88.5, 127.0], [88.6, 128.0], [88.7, 128.0], [88.8, 128.0], [88.9, 129.0], [89.0, 129.0], [89.1, 129.0], [89.2, 130.0], [89.3, 130.0], [89.4, 130.0], [89.5, 131.0], [89.6, 131.0], [89.7, 132.0], [89.8, 132.0], [89.9, 132.0], [90.0, 133.0], [90.1, 133.0], [90.2, 133.0], [90.3, 134.0], [90.4, 135.0], [90.5, 135.0], [90.6, 136.0], [90.7, 136.0], [90.8, 137.0], [90.9, 137.0], [91.0, 138.0], [91.1, 138.0], [91.2, 139.0], [91.3, 139.0], [91.4, 140.0], [91.5, 141.0], [91.6, 141.0], [91.7, 142.0], [91.8, 142.0], [91.9, 143.0], [92.0, 144.0], [92.1, 144.0], [92.2, 145.0], [92.3, 146.0], [92.4, 146.0], [92.5, 147.0], [92.6, 148.0], [92.7, 148.0], [92.8, 149.0], [92.9, 150.0], [93.0, 151.0], [93.1, 151.0], [93.2, 152.0], [93.3, 153.0], [93.4, 155.0], [93.5, 156.0], [93.6, 158.0], [93.7, 159.0], [93.8, 160.0], [93.9, 162.0], [94.0, 164.0], [94.1, 166.0], [94.2, 168.0], [94.3, 172.0], [94.4, 174.0], [94.5, 179.0], [94.6, 184.0], [94.7, 193.0], [94.8, 202.0], [94.9, 289.0], [95.0, 824.0], [95.1, 1075.0], [95.2, 1078.0], [95.3, 1079.0], [95.4, 1081.0], [95.5, 1082.0], [95.6, 1083.0], [95.7, 1084.0], [95.8, 1085.0], [95.9, 1087.0], [96.0, 1089.0], [96.1, 1091.0], [96.2, 1094.0], [96.3, 1096.0], [96.4, 1100.0], [96.5, 1104.0], [96.6, 1109.0], [96.7, 1119.0], [96.8, 1137.0], [96.9, 1177.0], [97.0, 2076.0], [97.1, 2078.0], [97.2, 2080.0], [97.3, 2082.0], [97.4, 2083.0], [97.5, 2084.0], [97.6, 2085.0], [97.7, 2087.0], [97.8, 2088.0], [97.9, 2090.0], [98.0, 2093.0], [98.1, 2095.0], [98.2, 2097.0], [98.3, 2101.0], [98.4, 2107.0], [98.5, 2115.0], [98.6, 2125.0], [98.7, 2141.0], [98.8, 3076.0], [98.9, 3080.0], [99.0, 3084.0], [99.1, 3088.0], [99.2, 3093.0], [99.3, 3097.0], [99.4, 3103.0], [99.5, 3118.0], [99.6, 3140.0], [99.7, 4081.0], [99.8, 4106.0], [99.9, 5106.0], [100.0, 75115.0]], "isOverall": false, "label": "suggestion_GET", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 0.0, "maxY": 26720.0, "series": [{"data": [[0.0, 26720.0], [700.0, 6.0], [12100.0, 1.0], [800.0, 16.0], [900.0, 7.0], [1000.0, 492.0], [1100.0, 196.0], [1200.0, 11.0], [1300.0, 2.0], [1400.0, 1.0], [100.0, 7960.0], [2000.0, 481.0], [2100.0, 181.0], [35000.0, 6.0], [35100.0, 7.0], [2200.0, 3.0], [2300.0, 1.0], [3000.0, 207.0], [3100.0, 118.0], [200.0, 49.0], [3200.0, 4.0], [3800.0, 1.0], [4000.0, 35.0], [4100.0, 20.0], [67000.0, 1.0], [4200.0, 3.0], [67100.0, 2.0], [4400.0, 2.0], [300.0, 19.0], [75000.0, 3.0], [75100.0, 7.0], [5000.0, 16.0], [5100.0, 11.0], [400.0, 2.0], [7000.0, 1.0], [500.0, 1.0]], "isOverall": false, "label": "suggestion_GET", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 75100.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 8.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 34747.0, "series": [{"data": [[0.0, 34747.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 729.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 1109.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 8.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 11.721518987341769, "minX": 1.6463133E12, "maxY": 50.0, "series": [{"data": [[1.64631336E12, 15.600902934537244], [1.64631354E12, 50.0], [1.64631372E12, 16.414879649890604], [1.64631342E12, 21.517691579943254], [1.6463136E12, 32.32734026745913], [1.6463133E12, 11.721518987341769], [1.64631348E12, 42.882795588083646], [1.64631366E12, 22.982619974059638]], "isOverall": false, "label": "sugestion_ConcurrencyThreadGroup-ThreadStarter", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64631372E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 84.72674418604653, "minX": 5.0, "maxY": 818.9541734860884, "series": [{"data": [[33.0, 147.74734042553195], [32.0, 114.35937500000001], [34.0, 175.87228260869557], [35.0, 125.21321321321325], [36.0, 150.76051188299823], [37.0, 182.70491803278696], [38.0, 173.54770992366403], [39.0, 168.01551724137937], [40.0, 210.34814814814808], [41.0, 158.80626780626793], [43.0, 150.0518234165067], [42.0, 183.0305810397554], [45.0, 818.9541734860884], [44.0, 224.08548707753505], [46.0, 201.06832298136655], [47.0, 216.1051212938005], [49.0, 222.7971014492754], [50.0, 369.44164952309615], [5.0, 101.25], [7.0, 85.5], [8.0, 140.0], [9.0, 128.0], [10.0, 226.66666666666669], [11.0, 86.51322751322752], [12.0, 87.01515151515159], [13.0, 84.72674418604653], [14.0, 85.89326765188822], [15.0, 100.78105696636894], [16.0, 102.03748981255085], [17.0, 126.86149936467602], [18.0, 130.94364051789788], [19.0, 123.51565074135065], [20.0, 188.35538752362962], [21.0, 146.7932551319648], [22.0, 127.4212765957447], [23.0, 141.22388059701504], [24.0, 127.71619496855342], [25.0, 149.76170510132795], [26.0, 163.9432565789473], [27.0, 153.15896739130434], [28.0, 221.37338629592844], [29.0, 240.95711060948088], [30.0, 171.07344632768368], [31.0, 189.03363518758093]], "isOverall": false, "label": "suggestion_GET", "isController": false}, {"data": [[33.64028639357257, 226.58388216325415]], "isOverall": false, "label": "suggestion_GET-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 50.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 251.18333333333334, "minX": 1.6463133E12, "maxY": 125089.26666666666, "series": [{"data": [[1.64631336E12, 28493.816666666666], [1.64631354E12, 83169.23333333334], [1.64631372E12, 29807.3], [1.64631342E12, 68149.28333333334], [1.6463136E12, 86769.01666666666], [1.6463133E12, 1142.4833333333333], [1.64631348E12, 125089.26666666666], [1.64631366E12, 49645.316666666666]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.64631336E12, 7059.35], [1.64631354E12, 20533.216666666667], [1.64631372E12, 7256.566666666667], [1.64631342E12, 16842.466666666667], [1.6463136E12, 21447.75], [1.6463133E12, 251.18333333333334], [1.64631348E12, 30916.233333333334], [1.64631366E12, 12285.733333333334]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64631372E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 102.13814898419885, "minX": 1.6463133E12, "maxY": 356.74033835170025, "series": [{"data": [[1.64631336E12, 102.13814898419885], [1.64631354E12, 356.74033835170025], [1.64631372E12, 128.38074398249455], [1.64631342E12, 125.02592242194882], [1.6463136E12, 297.71827637444346], [1.6463133E12, 103.37974683544302], [1.64631348E12, 205.70477270384575], [1.64631366E12, 208.87289234760013]], "isOverall": false, "label": "suggestion_GET", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64631372E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 102.11873589164802, "minX": 1.6463133E12, "maxY": 356.734750892443, "series": [{"data": [[1.64631336E12, 102.11873589164802], [1.64631354E12, 356.734750892443], [1.64631372E12, 121.08884026258224], [1.64631342E12, 125.01721854304618], [1.6463136E12, 297.71129271916766], [1.6463133E12, 103.31645569620252], [1.64631348E12, 205.6986908566129], [1.64631366E12, 208.86666666666648]], "isOverall": false, "label": "suggestion_GET", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64631372E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 41.06329113924051, "minX": 1.6463133E12, "maxY": 303.62393295048855, "series": [{"data": [[1.64631336E12, 53.47404063205417], [1.64631354E12, 303.62393295048855], [1.64631372E12, 73.67877461706776], [1.64631342E12, 75.99167455061468], [1.6463136E12, 246.50787518573506], [1.6463133E12, 41.06329113924051], [1.64631348E12, 149.42356458097103], [1.64631366E12, 156.69494163424122]], "isOverall": false, "label": "suggestion_GET", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64631372E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 69.0, "minX": 1.6463133E12, "maxY": 75115.0, "series": [{"data": [[1.64631336E12, 3081.0], [1.64631354E12, 67080.0], [1.64631372E12, 4092.0], [1.64631342E12, 35090.0], [1.6463136E12, 75115.0], [1.6463133E12, 444.0], [1.64631348E12, 35178.0], [1.64631366E12, 67100.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.64631336E12, 90.0], [1.64631354E12, 1087.0], [1.64631372E12, 104.0], [1.64631342E12, 92.0], [1.6463136E12, 119.0], [1.6463133E12, 141.0], [1.64631348E12, 146.0], [1.64631366E12, 99.40000000000009]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.64631336E12, 233.68000000000757], [1.64631354E12, 3147.119999999999], [1.64631372E12, 2081.4399999999996], [1.64631342E12, 2078.0], [1.6463136E12, 3078.6899999999996], [1.6463133E12, 444.0], [1.64631348E12, 3095.9799999999996], [1.64631366E12, 3080.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.64631336E12, 94.19999999999982], [1.64631354E12, 2085.0], [1.64631372E12, 118.0], [1.64631342E12, 104.0], [1.6463136E12, 145.0], [1.6463133E12, 163.0], [1.64631348E12, 187.0], [1.64631366E12, 152.0]], "isOverall": false, "label": "95th percentile", "isController": false}, {"data": [[1.64631336E12, 70.0], [1.64631354E12, 70.0], [1.64631372E12, 71.0], [1.64631342E12, 69.0], [1.6463136E12, 71.0], [1.6463133E12, 79.0], [1.64631348E12, 70.0], [1.64631366E12, 69.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.64631336E12, 82.0], [1.64631354E12, 94.0], [1.64631372E12, 89.0], [1.64631342E12, 82.0], [1.6463136E12, 87.0], [1.6463133E12, 88.0], [1.64631348E12, 98.0], [1.64631366E12, 85.0]], "isOverall": false, "label": "Median", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64631372E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 79.0, "minX": 1.0, "maxY": 630.0, "series": [{"data": [[3.0, 148.0], [4.0, 96.5], [5.0, 93.0], [8.0, 88.5], [9.0, 91.0], [10.0, 111.5], [11.0, 86.0], [12.0, 85.5], [13.0, 85.0], [14.0, 85.0], [15.0, 83.0], [16.0, 84.5], [17.0, 84.0], [18.0, 85.0], [20.0, 85.5], [22.0, 84.5], [23.0, 84.0], [24.0, 83.5], [25.0, 83.0], [26.0, 82.5], [27.0, 85.0], [28.0, 85.0], [29.0, 83.0], [30.0, 83.0], [31.0, 83.0], [32.0, 83.0], [34.0, 82.0], [35.0, 85.0], [36.0, 82.0], [38.0, 82.0], [41.0, 82.0], [40.0, 81.0], [42.0, 82.0], [43.0, 81.0], [44.0, 85.0], [45.0, 84.5], [46.0, 82.0], [47.0, 83.0], [49.0, 87.0], [48.0, 88.0], [50.0, 87.0], [51.0, 87.0], [53.0, 84.0], [52.0, 86.0], [54.0, 82.0], [55.0, 82.0], [57.0, 83.0], [56.0, 81.0], [58.0, 83.0], [59.0, 85.5], [61.0, 86.0], [60.0, 86.0], [63.0, 82.0], [62.0, 88.0], [64.0, 83.0], [66.0, 84.0], [65.0, 85.0], [67.0, 83.0], [68.0, 79.0], [70.0, 84.0], [71.0, 85.0], [69.0, 86.0], [72.0, 82.0], [75.0, 84.0], [74.0, 81.5], [73.0, 86.0], [76.0, 83.5], [79.0, 84.0], [77.0, 84.0], [78.0, 86.5], [80.0, 83.0], [83.0, 82.0], [81.0, 83.0], [82.0, 83.0], [85.0, 86.0], [86.0, 81.0], [84.0, 84.0], [88.0, 85.0], [89.0, 83.0], [91.0, 81.0], [90.0, 86.0], [92.0, 84.0], [93.0, 84.0], [95.0, 86.0], [94.0, 84.0], [96.0, 82.0], [99.0, 85.0], [97.0, 84.0], [101.0, 97.0], [102.0, 85.0], [100.0, 84.0], [103.0, 88.0], [105.0, 87.0], [107.0, 84.0], [106.0, 92.0], [108.0, 83.0], [109.0, 85.0], [110.0, 88.0], [111.0, 84.0], [115.0, 87.0], [112.0, 87.0], [114.0, 88.0], [113.0, 85.0], [118.0, 85.0], [119.0, 85.0], [117.0, 89.0], [116.0, 87.0], [121.0, 84.0], [123.0, 87.0], [122.0, 90.5], [120.0, 90.0], [125.0, 87.0], [127.0, 89.0], [124.0, 94.5], [128.0, 88.0], [133.0, 91.0], [135.0, 89.0], [132.0, 95.0], [131.0, 88.0], [129.0, 87.0], [130.0, 89.0], [137.0, 85.0], [138.0, 85.5], [141.0, 91.0], [140.0, 92.0], [136.0, 92.0], [143.0, 101.0], [139.0, 120.0], [145.0, 87.0], [147.0, 90.0], [150.0, 95.0], [144.0, 98.0], [153.0, 97.0], [152.0, 88.0], [157.0, 92.0], [154.0, 88.0], [159.0, 93.0], [158.0, 110.0], [155.0, 113.0], [163.0, 96.0], [160.0, 91.0], [165.0, 113.0], [167.0, 110.0], [170.0, 100.0], [172.0, 94.0], [169.0, 98.0], [168.0, 94.0], [179.0, 94.0], [183.0, 106.0], [180.0, 101.0], [186.0, 109.0], [190.0, 114.0], [189.0, 111.0], [187.0, 118.0], [184.0, 106.0], [193.0, 108.0], [195.0, 107.0], [199.0, 110.0], [197.0, 102.0], [200.0, 117.0], [203.0, 110.0], [201.0, 129.0], [212.0, 149.0], [1.0, 179.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[9.0, 630.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 212.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 0.0, "minX": 1.0, "maxY": 179.0, "series": [{"data": [[3.0, 148.0], [4.0, 96.5], [5.0, 92.0], [8.0, 88.5], [9.0, 91.0], [10.0, 111.5], [11.0, 86.0], [12.0, 85.5], [13.0, 85.0], [14.0, 85.0], [15.0, 83.0], [16.0, 84.5], [17.0, 84.0], [18.0, 85.0], [20.0, 85.5], [22.0, 84.5], [23.0, 84.0], [24.0, 83.5], [25.0, 83.0], [26.0, 82.5], [27.0, 85.0], [28.0, 84.5], [29.0, 83.0], [30.0, 83.0], [31.0, 83.0], [32.0, 83.0], [34.0, 82.0], [35.0, 85.0], [36.0, 82.0], [38.0, 82.0], [41.0, 82.0], [40.0, 81.0], [42.0, 82.0], [43.0, 81.0], [44.0, 85.0], [45.0, 84.5], [46.0, 82.0], [47.0, 83.0], [49.0, 87.0], [48.0, 88.0], [50.0, 87.0], [51.0, 87.0], [53.0, 83.5], [52.0, 86.0], [54.0, 82.0], [55.0, 82.0], [57.0, 83.0], [56.0, 81.0], [58.0, 82.5], [59.0, 85.5], [61.0, 86.0], [60.0, 86.0], [63.0, 82.0], [62.0, 88.0], [64.0, 83.0], [66.0, 84.0], [65.0, 85.0], [67.0, 83.0], [68.0, 79.0], [70.0, 84.0], [71.0, 85.0], [69.0, 86.0], [72.0, 82.0], [75.0, 84.0], [74.0, 81.5], [73.0, 86.0], [76.0, 83.5], [79.0, 84.0], [77.0, 84.0], [78.0, 86.5], [80.0, 83.0], [83.0, 82.0], [81.0, 83.0], [82.0, 83.0], [85.0, 86.0], [86.0, 81.0], [84.0, 84.0], [88.0, 85.0], [89.0, 83.0], [91.0, 81.0], [90.0, 86.0], [92.0, 84.0], [93.0, 84.0], [95.0, 86.0], [94.0, 84.0], [96.0, 82.0], [99.0, 85.0], [97.0, 84.0], [101.0, 97.0], [102.0, 85.0], [100.0, 84.0], [103.0, 88.0], [105.0, 87.0], [107.0, 84.0], [106.0, 92.0], [108.0, 83.0], [109.0, 85.0], [110.0, 88.0], [111.0, 84.0], [115.0, 87.0], [112.0, 87.0], [114.0, 88.0], [113.0, 85.0], [118.0, 85.0], [119.0, 85.0], [117.0, 89.0], [116.0, 87.0], [121.0, 84.0], [123.0, 87.0], [122.0, 90.5], [120.0, 90.0], [125.0, 87.0], [127.0, 89.0], [124.0, 94.5], [128.0, 88.0], [133.0, 91.0], [135.0, 89.0], [132.0, 95.0], [131.0, 88.0], [129.0, 87.0], [130.0, 89.0], [137.0, 85.0], [138.0, 85.5], [141.0, 91.0], [140.0, 92.0], [136.0, 92.0], [143.0, 101.0], [139.0, 120.0], [145.0, 87.0], [147.0, 90.0], [150.0, 95.0], [144.0, 98.0], [153.0, 97.0], [152.0, 88.0], [157.0, 92.0], [154.0, 88.0], [159.0, 93.0], [158.0, 110.0], [155.0, 113.0], [163.0, 96.0], [160.0, 91.0], [165.0, 113.0], [167.0, 110.0], [170.0, 100.0], [172.0, 93.5], [169.0, 98.0], [168.0, 94.0], [179.0, 94.0], [183.0, 106.0], [180.0, 101.0], [186.0, 109.0], [190.0, 114.0], [189.0, 111.0], [187.0, 118.0], [184.0, 106.0], [193.0, 108.0], [195.0, 107.0], [199.0, 110.0], [197.0, 102.0], [200.0, 117.0], [203.0, 110.0], [201.0, 129.0], [212.0, 149.0], [1.0, 179.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[9.0, 0.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 212.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.3333333333333333, "minX": 1.6463133E12, "maxY": 162.26666666666668, "series": [{"data": [[1.64631336E12, 37.05], [1.64631354E12, 107.38333333333334], [1.64631372E12, 38.016666666666666], [1.64631342E12, 88.18333333333334], [1.6463136E12, 111.6], [1.6463133E12, 1.3333333333333333], [1.64631348E12, 162.26666666666668], [1.64631366E12, 64.05]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64631372E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.06666666666666667, "minX": 1.6463133E12, "maxY": 161.68333333333334, "series": [{"data": [[1.64631372E12, 0.06666666666666667]], "isOverall": false, "label": "Non HTTP response code: javax.net.ssl.SSLException", "isController": false}, {"data": [[1.64631336E12, 36.916666666666664], [1.64631354E12, 107.38333333333334], [1.64631372E12, 37.95], [1.64631342E12, 88.08333333333333], [1.6463136E12, 112.16666666666667], [1.6463133E12, 1.3166666666666667], [1.64631348E12, 161.68333333333334], [1.64631366E12, 64.25]], "isOverall": false, "label": "200", "isController": false}, {"data": [[1.64631372E12, 0.06666666666666667]], "isOverall": false, "label": "Non HTTP response code: java.net.SocketException", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64631372E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.13333333333333333, "minX": 1.6463133E12, "maxY": 161.68333333333334, "series": [{"data": [[1.64631372E12, 0.13333333333333333]], "isOverall": false, "label": "suggestion_GET-failure", "isController": false}, {"data": [[1.64631336E12, 36.916666666666664], [1.64631354E12, 107.38333333333334], [1.64631372E12, 37.95], [1.64631342E12, 88.08333333333333], [1.6463136E12, 112.16666666666667], [1.6463133E12, 1.3166666666666667], [1.64631348E12, 161.68333333333334], [1.64631366E12, 64.25]], "isOverall": false, "label": "suggestion_GET-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64631372E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.13333333333333333, "minX": 1.6463133E12, "maxY": 161.68333333333334, "series": [{"data": [[1.64631336E12, 36.916666666666664], [1.64631354E12, 107.38333333333334], [1.64631372E12, 37.95], [1.64631342E12, 88.08333333333333], [1.6463136E12, 112.16666666666667], [1.6463133E12, 1.3166666666666667], [1.64631348E12, 161.68333333333334], [1.64631366E12, 64.25]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.64631372E12, 0.13333333333333333]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64631372E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

